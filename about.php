<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require("inc_head_php.php");
$title = "About";
require("inc_head_html.php");
?>

<h1>About QAGS Web</h1>

<p>
QAGS Web is written by <a href="https://rpg.phillipsuk.org/" target="_blank">Robin Phillips</a>. It is intended to be used by people playing online games of <a href="http://www.drivethrurpg.com/product/28315/QAGS-Second-Edition?it=1&affiliate_id=235519" target="_blank" title="QAGS on DriveThruRPG (affiliate link)">QAGS Second Edition</a> by <a href="http://www.hexgames.com/" target="_blank">Hex Games</a>. It can send updates (dice roll results etc) to an <a href="https://xmpp.org/" target="_blank">XMPP/Jabber</a> chatroom.
</p>
<p>
The source code is hosted on <a href="https://gitlab.com/robinphillips/qags-web" target="_blank">GitLab</a>. Please use the GitLab project to report bugs, request features, etc.
</p>

<h2>Copyright etc</h2>

<p>
QAGS Web is copyright (c) Robin Phillips. QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor.
</p>
<p>
You may freely use this software, or give it away to others. You may not charge for copies without the express permission of Hex Games and Robin Phillips. You may alter the code in any way you wish, and give away the changes. Bug reports, patches, etc are welcome. Submit them at the <a href="https://gitlab.com/robinphillips/qags-web">GitLab project</a>.
</p>
<p>
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
</p>

<?php
require("inc_foot.php");
?>

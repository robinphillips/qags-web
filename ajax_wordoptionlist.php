<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require_once("inc_head_php.php");

// Load a list of words into a <SELECT>
$ajaxdb = new SQLite3(DBFILE);
$charid = intval($_GET["charid"]);

// Output an <OPTION> tag, selected if it is the default
function echo_option($value, $display) {
	echo "<option";
	if (isset($_GET["default"]) && $_GET["default"] == $value)
		echo " selected";
	echo " value='".htmlentities($value, ENT_QUOTES)."'";
	echo ">".htmlentities($display, ENT_QUOTES)."</option>";
}

$sql = "SELECT body, brain, nerve FROM characters WHERE charid = $charid";
$bbn = $db->querySingle($sql, True);
echo_option("body", "Body (".$bbn["body"].")");
echo_option("brain", "Brain (".$bbn["brain"].")");
echo_option("nerve", "Nerve (".$bbn["nerve"].")");

echo "<optgroup label='Jobs'>";
$sql = "SELECT * FROM words WHERE charid = $charid AND type LIKE 'job' ORDER BY value DESC";
$words = $ajaxdb->query($sql);
while ($word = $words->fetchArray(SQLITE3_ASSOC))
	echo_option($word["wordid"], $word["word"]." (".$word["value"].")");
echo "</optgroup>";

echo "<optgroup label='Gimmicks'>";
$sql = "SELECT * FROM words WHERE charid = $charid AND type LIKE 'gimmick' ORDER BY value DESC";
$words = $ajaxdb->query($sql);
while ($word = $words->fetchArray(SQLITE3_ASSOC))
	echo_option($word["wordid"], $word["word"]." (".$word["value"].")");

echo "<optgroup label='Weaknesses'>";
$sql = "SELECT * FROM words WHERE charid = $charid AND type LIKE 'weakness' ORDER BY value DESC";
$words = $ajaxdb->query($sql);
while ($word = $words->fetchArray(SQLITE3_ASSOC))
	echo_option($word["wordid"], $word["word"]." (".$word["value"].")");
echo "</optgroup>";

$ajaxdb->close();
?>

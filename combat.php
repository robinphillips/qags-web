<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require("inc_head_php.php");
require("inc_head_html.php");
// Initialise $log and $winner
$log = "";
$winner = 0;

if (isset($_POST["btnSubmitYY"]) && $_POST["btnSubmitYY"] != "") {
	$msg = "<p>";
	$damage = intval($_POST["damage"]);
	if ($_POST["winner"] == 1) {
		if ($_POST["yumyums1"] > 0) {
			// Character 1 is increasing damage
			$char2hp = $db->querySingle("SELECT currenthp FROM characters WHERE charid=".intval($_POST["character2"]));
			$damage += (intval($_POST["yumyums1"]) * 3);
			if ($damage > $char2hp)
				$damage = $char2hp;
			$char2hp = $char2hp - $damage;
			if ($char2hp < 0)
				$char2hp = 0;
			$sql = "UPDATE characters SET currenthp = $char2hp WHERE charid=".intval($_POST["character2"]);
			$db->exec($sql);
			$sql = "UPDATE characters SET yumyums = yumyums-".intval($_POST["yumyums1"])." WHERE charid=".intval($_POST["character1"]);
			$db->exec($sql);
			$msg .= $_POST["htmlchar1"]." has spent ".intval($_POST["yumyums1"])." Yum Yums to increase damage to ".$_POST["htmlchar2"]." by ".intval($_POST["yumyums1"]) * 3 ."<br>";
			$msg .= $_POST["htmlchar2"]." now has $char2hp HP<br>";
		}
		if ($_POST["yumyums2"] > 0) {
			// Character 2 is decreasing damage
			$decrease = $_POST["yumyums2"] * 3;
			if ($decrease > $damage)
				$decrease = $damage;
			$char2 = $db->querySingle("SELECT hp, currenthp FROM characters WHERE charid=".intval($_POST["character2"]), True);
			$char2hp = $char2["currenthp"] + $decrease;
			if ($char2hp > $char2["hp"])
				$char2hp = $char2["hp"];
			$sql = "UPDATE characters SET currenthp = $char2hp WHERE charid=".intval($_POST["character2"]);
			$db->exec($sql);
			$sql = "UPDATE characters SET yumyums = yumyums-".intval($_POST["yumyums2"])." WHERE charid=".intval($_POST["character2"]);
			$db->exec($sql);
			$msg .= $_POST["htmlchar2"]." has spent ".intval($_POST["yumyums2"])." Yum Yums to decrease damage by $decrease<br>";
			$msg .= $_POST["htmlchar2"]." now has $char2hp HP<br>";
		}
	}

	if ($_POST["winner"] == 2) {
		if ($_POST["yumyums2"] > 0) {
			// Character 2 is increasing damage
			$char1hp = $db->querySingle("SELECT currenthp FROM characters WHERE charid=".intval($_POST["character1"]));
			$damage += (intval($_POST["yumyums2"]) * 3);
			if ($damage > $char1hp)
				$damage = $char1hp;
			$char1hp = $char1hp - $damage;
			if ($char1hp < 0)
				$char1hp = 0;
			$sql = "UPDATE characters SET currenthp = $char1hp WHERE charid=".intval($_POST["character1"]);
			$db->exec($sql);
			$sql .= "UPDATE characters SET yumyums = yumyums-".intval($_POST["yumyums2"])." WHERE charid=".intval($_POST["character2"]);
			$db->exec($sql);
			$msg .= $_POST["htmlchar2"]." has spent ".intval($_POST["yumyums2"])." Yum Yums to increase damage to ".$_POST["htmlchar1"]." by ".intval($_POST["yumyums2"]) * 3 ."<br>";
			$msg .= $_POST["htmlchar1"]." now has $char1hp HP<br>";
		}
		if ($_POST["yumyums1"] > 0) {
			// Character 1 is decreasing damage
			$decrease = $_POST["yumyums1"] * 3;
			if ($decrease > $damage)
				$decrease = $damage;
			$char1 = $db->querySingle("SELECT hp, currenthp FROM characters WHERE charid=".intval($_POST["character1"]), True);
			$char1hp = $char1["currenthp"] + $decrease;
			if ($char1hp > $char1["hp"])
				$char1hp = $char1["hp"];
			$sql = "UPDATE characters SET currenthp = $char1hp WHERE charid=".intval($_POST["character1"]);
			$db->exec($sql);
			$sql .= "UPDATE characters SET yumyums = yumyums-".intval($_POST["yumyums1"])." WHERE charid=".intval($_POST["character1"]);
			$db->exec($sql);
			$msg .= $_POST["htmlchar1"]." has spent ".intval($_POST["yumyums1"])." Yum Yums to decrease damage by $decrease<br>";
			$msg .= $_POST["htmlchar1"]." now has $char1hp HP<br>";
		}
	}
	
	$msg .= "</p>";
	logdb($msg);
	echo "<div class='box' id='results'><p class='boxtitle'>Yum Yums Spent</p>\n";
	echo "<p>$msg</p>";
	echo "</div>\n";
}

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {

	// Character 1 details
	$char1 = $db->querySingle("SELECT name, currenthp, yumyums FROM characters WHERE charid = ".intval($_POST["character1"]), True);
	$char1hp = intval($char1["currenthp"]);
	
	$htmlchar1 = htmlentities($char1["name"], ENT_QUOTES);
	if ($_POST["word1"] == "body" || $_POST["word1"] == "brain" || $_POST["word1"] == "nerve") {
		$word1name = htmlentities(ucwords($_POST["word1"]), ENT_QUOTES);
		$word1value = $db->querySingle("SELECT ".$_POST["word1"]." FROM characters WHERE charid = ".intval($_POST["character1"]));
	}
	else {
		$word = $db->querySingle("SELECT * FROM words WHERE wordid = ".intval($_POST["word1"]), True);
		$word1name = htmlentities($word["word"], ENT_QUOTES);
		$word1value = $word ["value"];
	}

	// Character 2 details
	$char2 = $db->querySingle("SELECT name, currenthp, yumyums FROM characters WHERE charid = ".intval($_POST["character2"]), True);
	$char2hp = intval($char2["currenthp"]);

	$htmlchar2 = htmlentities($char2["name"], ENT_QUOTES);
	if ($_POST["word2"] == "body" || $_POST["word2"] == "brain" || $_POST["word2"] == "nerve") {
		$word2name = htmlentities(ucwords($_POST["word2"]), ENT_QUOTES);
		$word2value = $db->querySingle("SELECT ".$_POST["word2"]." FROM characters WHERE charid = ".intval($_POST["character2"]));
	}
	else {
		$word = $db->querySingle("SELECT * FROM words WHERE wordid = ".intval($_POST["word2"]), True);
		$word2name = htmlentities($word["word"], ENT_QUOTES);
		$word2value = $word ["value"];
	}

	// Log results
	if (isset ($_POST["ranged"]))
		$log .= "$htmlchar1 ($word1name $word1value) making a ranged attack against $htmlchar2 ($word2name $word2value)<br>";
	else
		$log .= "$htmlchar1 ($word1name $word1value) fighting $htmlchar2 ($word2name $word2value)<br>";
	if (isset ($_POST["note"]) and $_POST["note"] != "")
		$log .= "<i>Note: " . htmlentities($_POST["note"], ENT_QUOTES) . "</i><br>";

	// Default roll (character 1)
	if (isset ($_POST["default1"])) {
		$word1value = floor ($word1value/2);
		$log .= "$htmlchar1 is making a default roll, so Number is $word1value<br>";
	}
	// Skills (character 1)
	foreach ($_POST as $key=>$value) {
		if (substr ($key, 0, 11) == "chkskills1_") {
			$skillid = substr ($key, 11);
			$skillname = htmlentities($db->querySingle("SELECT word FROM words WHERE wordid = $skillid"), ENT_QUOTES);
			$log .= "$htmlchar1 is using skill $skillname (+$value)<br>";
			$word1value += $value;
		}
	}
	// Other modifier (character 1)
	if (isset($_POST["modifier1"]) && intval($_POST["modifier1"]) != 0) {
		$log .= "$htmlchar1 modifier: ".intval($_POST["modifier1"])."<br>";
		$word1value += intval($_POST["modifier1"]);
	}
	
	// Default roll (character 2)
	if (isset ($_POST["default2"])) {
		$word2value = floor ($word2value/2);
		$log .= "$htmlchar2 is making a default roll, so Number is $word2value<br>";
	}
	// Skills (character 2)
	foreach ($_POST as $key=>$value) {
		if (substr ($key, 0, 11) == "chkskills2_") {
			$skillid = substr ($key, 11);
			$skillname = htmlentities($db->querySingle("SELECT word FROM words WHERE wordid = $skillid"), ENT_QUOTES);
			$log .= "$htmlchar2 is using skill $skillname (+$value)<br>";
			$word2value += $value;
		}
	}
	// Other modifier (character 2)
	if (isset($_POST["modifier2"]) && intval($_POST["modifier2"]) != 0) {
		$log .= "$htmlchar2 modifier: ".intval($_POST["modifier2"])."<br>";
		$word2value += intval($_POST["modifier2"]);
	}
	
	// Roll dice
	$dieroll1 = mt_rand (1, 20);
	$dieroll2 = mt_rand (1, 20);
	
	$log .= "$htmlchar1 rolls $dieroll1 (";
	if ($dieroll1 <= $word1value)
		$log .= "Success";
	else
		$log .= "Failed";
	$log .= ")<br>";
	
	$log .= "$htmlchar2 rolls $dieroll2 (";
	if ($dieroll2 <= $word2value)
		$log .= "Success";
	else
		$log .= "Failed";
	$log .= ")<br>";
	
	// Results
	$winner = 0;
	$damage = 0;
	$showyy = True;
	if ($dieroll1 > $word1value && $dieroll2 > $word2value)
		$log .= "<p class='bad'>Both characters failed their roll.</p>";
	elseif ($dieroll1 <= $word1value && $dieroll2 > $word2value) {
		$winner = 1;
		$damage = $dieroll1;
	}
	elseif ($dieroll1 > $word1value && $dieroll2 <= $word2value) {
		$winner = 2;
		$damage = $dieroll2;
	}
	elseif ($dieroll1 <= $word1value && $dieroll2 <= $word2value) {
		if ($dieroll1 > $dieroll2) {
			$winner = 1;
			$damage = $dieroll1 - $dieroll2;
		}
		elseif ($dieroll1 < $dieroll2) {
			$winner = 2;
			$damage = $dieroll2 - $dieroll1;
		}
		elseif ($dieroll1 == $dieroll2)
			$log .= "<p class='bad'>Both rolls succeeded. Result is a tie. GM may declare a stalemate or have the players reroll. See QAGS page 20</p>";
	}

	if ($winner == 1) {
		// Modify damage
		if (isset($_POST["damage1"]) && intval($_POST["damage1"]) != 0) {
			$damage += intval($_POST["damage1"]);
			$log .= "Damage modifier: ".intval($_POST["damage1"])."<br>";
		}
		if (isset($_POST["armour2"]) && intval($_POST["armour2"]) != 0) {
			$damage -= intval($_POST["armour2"]);
			$log .= "$htmlchar2 has armour (".intval($_POST["armour2"]).")<br>";
		}

		$log .= "<p class='good'>$htmlchar1 wins!<br></p>\n";
		if ($damage > $char2hp)
			$damage = $char2hp;
		$newhp = $char2hp - $damage;
		// Update database
		$sql = "UPDATE characters SET currenthp = $newhp WHERE charid=".intval($_POST["character2"]);
		$db->exec($sql);
		$log .= "<p>$htmlchar2 takes $damage points of damage. Health is reduced from $char2hp to $newhp</p>";
	}
	elseif ($winner == 2) {
		if (isset ($_POST["ranged"]))
			$log .= "<p class='bad'>$htmlchar1 misses!</p>";
		else {
			// Modify damage
			if (isset($_POST["damage2"]) && intval($_POST["damage2"]) != 0) {
				$damage += intval($_POST["damage2"]);
				$log .= "Damage modifier: ".intval($_POST["damage2"])."<br>";
			}
			if (isset($_POST["armour1"]) && intval($_POST["armour1"]) != 0) {
				$damage -= intval($_POST["armour1"]);
				$log .= "$htmlchar1 has armour (".intval($_POST["armour1"]).")<br>";
			}
			
			$log .= "<p class='good'>$htmlchar2 wins!<br></p>";
			if ($damage > $char1hp)
				$damage = $char1hp;
			$newhp = $char1hp - $damage;
			// Update database
			$sql = "UPDATE characters SET currenthp = $newhp WHERE charid=".intval($_POST["character1"]);
			$db->exec($sql);
			$log .= "<p>$htmlchar1 takes $damage points of damage. Health is reduced from $char1hp to $newhp</p>";
		}
	}
	
	// Log the result
	logdb ($log);
}

// Defaults for AJAX
if (isset($_POST["word1"]))
	$word1default = urlencode($_POST["word1"]);
else
	$word1default = "";
if (isset($_POST["word2"]))
	$word2default = urlencode($_POST["word2"]);
else
	$word2default = "";
?>

<script>
$(function() {
	// Get word lists at load
	$('#word1').load('./ajax_wordoptionlist.php?charid='+$("#character1").val()+'&default=<?=$word1default;?>')
	$('#skills1').load('./ajax_skillcheckboxlist.php?charid='+$("#character1").val()+'&basename=chkskills1_')
	$('#hp1').load('./ajax_hp.php?charid='+$("#character1").val())
	$('#word2').load('./ajax_wordoptionlist.php?charid='+$("#character2").val()+'&default=<?=$word2default;?>')
	$('#skills2').load('./ajax_skillcheckboxlist.php?charid='+$("#character2").val()+'&basename=chkskills2_')
	$('#hp2').load('./ajax_hp.php?charid='+$("#character2").val())
	
	// Update word lists when character changes
	$("#character1").change(function(event) {
		$('#word1').load('./ajax_wordoptionlist.php?charid='+$("#character1").val()+'&default=<?=$word1default;?>')
		$('#skills1').load('./ajax_skillcheckboxlist.php?charid='+$("#character1").val()+'&basename=chkskills1_')
		$('#hp1').load('./ajax_hp.php?charid='+$("#character1").val())
	});
	$("#character2").change(function(event) {
		$('#word2').load('./ajax_wordoptionlist.php?charid='+$("#character2").val()+'&default=<?=$word2default;?>')
		$('#skills2').load('./ajax_skillcheckboxlist.php?charid='+$("#character2").val()+'&basename=chkskills2_')
		$('#hp2').load('./ajax_hp.php?charid='+$("#character2").val())
	});
	
	// Hide results & yum yums boxes on rolling dice
	$("#btnSubmit").click(function(event) {
		$("#results").hide()
		$("#yumyums").hide()
	})
	
	// Ranged combat or not
	$("#ranged").change(function(event) {
		if ($("#ranged").prop("checked")) {
			$("#char1title").text("Attacker")
			$("#char2title").text("Defender")
		}
		else {
			$("#char1title").text("Character 1")
			$("#char2title").text("Character 2")
		}
	})
	
	// Show amount damage will be increased/decreased when spending Yum Yums
	winner = <?=$winner;?>;
	$(".yumyums").change(function(event) {
		yy=$(this).data("number")
		if ($(this).val() == 0)
			$("#yydamage"+yy).text("")
		else {
			if (winner == yy)
				txt="Increase"
			else
				txt="Decrease"
			txt += " damage by "+$(this).val()*3
			$("#yydamage"+yy).text(txt)
		}
	})
})
</script>

<h1>Combat</h1>

<form method="post">
<div class="box">
<p class="boxtitle" id="char1title">Character 1</p>
<p>
<select name="character1" id="character1">
<?php
if (isset($_POST["character1"]))
	selectCharacters($db, intval($_POST["character1"]));
else
	selectCharacters($db, CHARACTERID);
?>
</select><br>
Word: <select name="word1" id="word1">
</select>
</p>
<p>
Current HP: <span id="hp1"></span>
</p>
<p class="boxtitle">Skills</p>
<p id="skills1">
</p>
<p class="boxtitle">Other</p>

<?php
if (isset($_POST["default1"]))
	$checked=" checked";
else
	$checked="";
if (isset($_POST["armour1"]))
	$armour = intval($_POST["armour1"]);
else
	$armour = "";
if (isset($_POST["damage1"]))
	$damagebonus = intval($_POST["damage1"]);
else
	$damagebonus = "";
if (isset($_POST["modifier1"]))
	$modifier = intval($_POST["modifier1"]);
else
	$modifier = "";
?>
<p>
<input type="checkbox" id="default1" name="default1"<?=$checked;?>> <label for="default1">This is a default roll</label><br>
Armour: <input class="small" name="armour1" value="<?=$armour;?>" type="number"><br>
Damage bonus: <input class="small" name="damage1" value="<?=$damagebonus;?>" type="number"><br>
Other modifier to hit: <input class="small" name="modifier1" value="<?=$modifier;?>" type="number">
</p>
</div>

<div class="box">
<p class="boxtitle" id="char2title">Character 2</p>
<p>
<select name="character2" id="character2">
<?php
selectCharacters($db, intval($_POST["character2"]));
?>
</select><br>
Word: <select name="word2" id="word2">
</select>
</p>
<p>
Current HP: <span id="hp2"></span>
</p>
<p class="boxtitle">Skills</p>
<p id="skills2">
</p>
<p class="boxtitle">Other</p>

<?php
if (isset($_POST["default2"]))
	$checked=" checked";
else
	$checked="";
if (isset($_POST["armour2"]))
	$armour = intval($_POST["armour2"]);
else
	$armour = "";
if (isset($_POST["damage2"]))
	$damagebonus = intval($_POST["damage2"]);
else
	$damagebonus = "";
if (isset($_POST["modifier2"]))
	$modifier = intval($_POST["modifier2"]);
else
	$modifier = "";
?>
<p>
<input type="checkbox" id="default2" name="default2"<?=$checked;?>> <label for="default2">This is a default roll</label><br>
Armour: <input class="small" name="armour2" value="<?=$armour;?>" type="number"><br>
Damage bonus: <input class="small" name="damage2" value="<?=$damagebonus;?>" type="number"><br>
Other modifier to hit: <input class="small" name="modifier2" value="<?=$modifier;?>" type="number">
</p>
</div>

<?php
if (isset($_POST["ranged"]))
	$checked=" checked";
else
	$checked="";
if (isset($_POST["note"]))
	$note=htmlentities($_POST["note"], ENT_QUOTES);
else
	$note="";
?>
<p>
<input type="checkbox" id="ranged" name="ranged"<?=$checked;?>> <label for="ranged">Ranged combat (only the attacker can cause damage in ranged combat)</label><br>
Note: <input name="note" class="mid" value="<?=$note;?>">
</p>

<input type="submit" name="btnSubmit" id="btnSubmit" value="Roll the dice">
</form>

<?php
if ($log != "") {
	// Show results
	echo "<div class='box' id='results'><p class='boxtitle'>Results</p>\n";
	echo "<p>$log</p>";
	echo "</div>\n";

	if ($winner != 0) {
		echo "<div class='box' id='yumyums'><p class='boxtitle'>YumYums</p>\n";
		echo "<form method='post'>";
		echo "<p>Increase or decrease damage by three for every Yum Yum spent</p>";
		echo "<p>$htmlchar1: ";
		echo "<select name='yumyums1' class='yumyums' data-number='1'>";
		for ($i=0; $i<=$char1["yumyums"]; $i++)
			echo "<option value='$i'>$i</option>";
		echo "</select> <span id='yydamage1'></span></p>";
		echo "<p>$htmlchar2: ";
		echo "<select name='yumyums2' class='yumyums' data-number='2'>";
		for ($i=0; $i<=$char2["yumyums"]; $i++)
			echo "<option value='$i'>$i</option>";
		echo "</select> <span id='yydamage2'></span></p>";
		echo "<span id='moddamage'></span></p>";
		echo "<input type='hidden' name='htmlchar1' value='$htmlchar1'>";
		echo "<input type='hidden' name='htmlchar2' value='$htmlchar2'>";
		echo "<input type='hidden' name='character1' value='".intval($_POST["character1"])."'>";
		echo "<input type='hidden' name='character2' value='".intval($_POST["character2"])."'>";
		echo "<input type='hidden' name='winner' value='$winner'>";
		echo "<input type='hidden' name='damage' value='$damage'>";
		echo "<p><input type='submit' value='Spend Yum Yums' name='btnSubmitYY'></p></form>";
		echo "</div>\n";
	}
}

require("inc_foot.php");
?>

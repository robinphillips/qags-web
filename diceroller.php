<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

/*
Roll dice
*/
function dice_roll ($dice, $sides=20) {
	// Initialise result array
	$result = array ();
	if ($dice == 0)
		$result[] = 0;
	else
		for ($i = 1; $i <= $dice; $i++) {
			if ($sides == "Average") {
				$roll = mt_rand(1, 6);
				if ($roll == 1)
					$roll = 3;
				if ($roll == 6)
					$roll = 4;
			}
			else
				$roll = mt_rand(1, $sides);
			$result[] = $roll;
		}

	return $result;
}

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	setcookie("qw_drnumber", intval($_POST["number"]), time()+60*60*24*30);
	setcookie("qw_drsides", intval($_POST["sides"]), time()+60*60*24*30);
	setcookie("qw_drnote", $_POST["note"]);

	$numdice = intval($_POST["number"]);
	if ($numdice < 1)
		$numdice = 1;
}
require("inc_head_php.php");
require("inc_head_html.php");

// Get defaults
if (isset ($_POST["number"]))
	$drnumber = $numdice;
elseif (isset ($_COOKIE["qw_drnumber"]))
	$drnumber = intval($_COOKIE["qw_drnumber"]);
else
	$drnumber = 1;

if (isset ($_POST["number"]))
	$drsides = intval($_POST["sides"]);
elseif (isset ($_COOKIE["qw_drnumber"]))
	$drsides = intval($_COOKIE["qw_drsides"]);
else
	$drsides = 20;

if (isset ($_POST["note"]))
	$drnote = $_POST["note"];
elseif (isset ($_COOKIE["qw_drnote"]))
	$drnote = $_COOKIE["qw_drnote"];
else
	$drnote = "";
?>

<script>
$(function() {
	// Hide results box on rolling dice
	$("#btnSubmit").click(function(event) {
		$("#results").hide()
	})
})
</script>

<h1>Dice Roller</h1>

<p>
Roll the selected number of dice. Individual values and total will be displayed.
</p>

<form method="post">
<p>
<input name="number" class="small" value="<?=$drnumber;?>" required type="number">
<select name="sides">
<?php
$dice=array(4, 6, "Average", 8, 10, 12, 20, 100);
foreach ($dice as $die) {
	echo "<option value='$die'";
	if ($die == $drsides)
		echo " selected";
	echo ">D$die</option>";
}
?>
</select>
</p>

<p>
Note: <input name="note" class="mid" value="<?=htmlentities($drnote, ENT_QUOTES);?>">
</p>

<p>
<input type="submit" value="Roll" name="btnSubmit" id="btnSubmit">
</p>
</form>

<?php
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	// Roll the dice
	$roll = dice_roll($numdice, intval($_POST["sides"]));
	$log .= "<p>".PLAYERNAME." rolling $numdice D".htmlentities($_POST["sides"], ENT_QUOTES)."<br>";
	if ($drnote != "")
		$log .= "<i>Note: " . htmlentities($drnote, ENT_QUOTES) . "</i><br>";
	$log .= "Rolls: ";
	foreach ($roll as $die)
		$log .= "$die, ";
	// Remove final comma-space
	$log = substr($log, 0, -2) . "<br>";
	// Total
	$log .= "Total: " . array_sum($roll) . "</p>";

	// Log the result
	logdb ($log);

	// Show results
	echo "<div class='box' id='results'><h2>Results</h2>\n";
	echo "$log</div>\n";
}

require("inc_foot.php");
?>

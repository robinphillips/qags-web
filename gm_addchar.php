<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/
require("inc_head_php.php");
require("inc_head_html.php");

$msg = "";
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	// Assume everything will work. $msg will be changed if there is a problem
	$msg = "<p class='good'>Added ".htmlentities($_POST["name"], ENT_QUOTES).".</p>";
	
	// Set up SQL to insert main stats into characters table
	$sql = "INSERT INTO characters ('gmc', 'name', 'body', 'brain', 'nerve', 'hp', 'currenthp', 'yumyums', 'concept', 'tagline', 'dumbfact', 'notes', 'gmnotes', 'wwphitm', 'active') VALUES (";
	if (isset($_POST["gmc"]))
		$sql .= "1, ";
	else
		$sql .= "0, ";
	$sql .= "'".$db->escapeString($_POST["name"])."',
		".intval($_POST["body"]).",
		".intval($_POST["brain"]).",
		".intval($_POST["nerve"]).",
		".intval($_POST["hp"]).",
		".intval($_POST["hp"]).",
		".intval($_POST["yumyums"]).",
		'".$db->escapeString($_POST["concept"])."',
		'".$db->escapeString($_POST["tagline"])."',
		'".$db->escapeString($_POST["dumbfact"])."',
		'".$db->escapeString($_POST["notes"])."',
		'".$db->escapeString($_POST["gmnotes"])."',
		'".$db->escapeString($_POST["wwphitm"])."',
		1)";

	if ($db->exec($sql) === False)
		$msg = "Errors inserting character into database.";
	else {
		$charid = $db->lastInsertRowID();
		
		// Go through words
		foreach ($_POST as $key=>$val) {
			if (substr($key, 0, 7) == "word_t_") {
				$index = intval(substr($key, 7));
				$sql = "INSERT INTO words ('charid', 'type', 'word', 'value') VALUES (
					$charid,
					'".$db->escapeString($val)."',
					'".$db->escapeString($_POST["word_w_$index"])."',
					".intval($_POST["word_v_$index"]).
					")";
				if ($db->exec($sql) === False)
					$msg = "<p class='bad'>Errors inserting character into database.</p>";
			}
		}

		// Go through words
		foreach ($_POST as $key=>$val) {
			if (substr($key, 0, 5) == "group") {
				$index = intval(substr($key, 7));
				$sql = "INSERT INTO groupmembers ('gm_groupid', 'gm_charid') VALUES (
					".intval(substr($key, 5)).",
					$charid
					)";
				if ($db->exec($sql) === False)
					$msg = "<p class='bad'>Errors inserting character into database.</p>";
			}
		}

		// Update player table if a player was selected
		if (intval($_POST["player"] != 0)) {
			$sql = "UPDATE players SET player_charid = $charid WHERE playerid = ".intval($_POST["player"]);
			$db->exec($sql);
		}
	}
}
?>

<script>
$(function() {
	$(".add").click (function() {
		wordtype = $(this).data("type")

		number = $("#wordsdiv").data("count")
		$("#wordsdiv").data("count", ++number)
		
		if (wordtype == "Skill")
			plus = "+"
		else
			plus = ""
		
		h = "<div class='word'><input type='hidden' name='word_t_"+number+"' value='"+wordtype+"'>"+wordtype+": "
		h += "<input name='word_w_"+number+"' id='word_w_"+number+"' class='mid'>"
		h += "<span class='donotwrap'>"+plus+"<input name='word_v_"+number+"' class='small' type='number'></span></div>"
		$("#wordsdiv").append(h)
		$("#word_w_"+number).focus()
	})
})
</script>

<h1>Add Character</h1>

<?php
echo $msg;
?>

<form method="post">
<p>
Player: <select name="player">
<option value="0">None</option>
<?php
$sql = "SELECT playerid, name FROM players ORDER BY name";
$players = $db->query($sql);
while ($player = $players->fetchArray(SQLITE3_ASSOC))
	echo "<option value='".$player["playerid"]."'>".htmlentities($player["name"], ENT_QUOTES)."</option>";
?>
</select>
</p>

<div class="box">
Name: <input name="name" required><br>
Concept: <input name="concept"><br>
Tag Line: <input name="tagline"><br>
<div class='innerthird'>
Body: <input name="body" class="small" required type="number"><br>
Brain: <input name="brain" class="small" required type="number"><br>
</div>
<div class='innerthird'>
Nerve: <input name="nerve" class="small" required type="number"><br>
Yum Yums: <input name="yumyums" class="small" required type="number"><br>
</div>
<div class='innerthird'>
Health: <input name="hp" class="small" required type="number"><br>
<input type="checkbox" id="gmc" name="gmc" title="Tick this box if the character is a GMC"> <label for="gmc" title="Tick this box if the character is a GMC">This is a GMC</label>
</div>
</div>

<h2>Words</h2>
<div class="box">
<div id="wordsdiv" data-count="0"></div>
<p>Add a: <a href="#" class="add" data-type="Job">Job</a>
<a href="#" class="add" data-type="Gimmick">Gimmick</a>
<a href="#" class="add" data-type="Weakness">Weakness</a>
<a href="#" class="add" data-type="Skill">Skill</a></p>
</div>

<h2>Notes etc.</h2>
<div class="box">
<p>
Dumb Fact: <input name="dumbfact"><br>
WWPHITM: <input name="wwphitm"><br>
Notes:<br>
<textarea name="notes"></textarea><br>
GM Notes:<br>
<textarea name="gmnotes"></textarea>
</p>
</div>

<h2>Groups</h2>
<div class="box">
	<?php
	$sql = "SELECT * FROM groups ORDER by groupname";
	$groups = $db->query($sql);
	while ($group = $groups->fetchArray(SQLITE3_ASSOC)) {
		echo "<input type='checkbox' name='group".$group["groupid"]."' id='g".$group["groupid"]."'><label for='g".$group["groupid"]."'> ".htmlentities($group["groupname"], ENT_QUOTES)."</label>&nbsp;";
	}
	?>
</div>

<p>
<input type="submit" name="btnSubmit" value="Add character">
</p>
</form>

<?php
require("inc_foot.php");
?>

<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require("inc_head_php.php");
require("inc_head_html.php");
$updated = "";

if (isset($_POST["btnSubmit"])) {
	// Get value of GM checkbox
	if (isset($_POST["gm"]) && $_POST["gm"] == 1)
		$gm = 1;
	else
		$gm = 0;
	// Get password hash
	if (isset($_POST["password"]) && $_POST["password"] != "")
		$pwhash = $db->escapeString(password_hash ($_POST["password"], PASSWORD_DEFAULT));
	else
		$pwhash = False;
}

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] == "Add Player") {
	// Set up and run INSERT query
	$sql = "INSERT INTO players (name, player_charid, password, gm, email) VALUES (
		'".$db->escapeString($_POST["name"])."',
		".$_POST["character"].",
		'$pwhash',
		$gm,
		'".$db->escapeString($_POST["email"])."')";
	$db->exec($sql);
	$updated = "Player details updated.";
}
elseif (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] == "Update Player") {
	// Set up SQL to update password if required
	if ($pwhash !== False)
		$passwordsql = "password = '$pwhash',";
	else
		$passwordsql = "";
	// Set up and run UPDATE query
	$sql = "UPDATE players SET
		name = '".$db->escapeString($_POST["name"])."',
		email = '".$db->escapeString($_POST["email"])."',
		$passwordsql
		player_charid = ".$_POST["character"].",
		gm = $gm
		WHERE playerid = ".intval($_POST["playerid"]);
	$db->exec($sql);
	
	$updated = "Player details updated.";
}
elseif (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] == "Remove Player") {
	// Set up and run DELETE query
	$sql = "DELETE FROM players
		WHERE playerid = ".intval($_POST["playerid"]);
	$db->exec($sql);
	
	$updated = "Player removed.";
}
?>

<script>
$(function() {
	// Show add controls
	$("#addnewplayer").click(function() {
		// Clear form
		$(".aeinput").val("")
		$("#character").val(0)
		$("#gm").prop("checked", false)
		// Set submit button text
		$("#btnSubmit").val("Add Player")
		// Title
		$("#aetitle").text("Add New Player")
		$("#aepassword").text("Set")
		// Hide "Remove player" checkbox
		$("#aeremove").hide()
		// Show form
		$("#editform").show()
	})
	
	// Show edit controls
	$(".editbutton").click(function() {
		// Get player's ID
		pid=$(this).data("pid")
		$("#playerid").val(pid)
		// Clear form
		$(".aeinput").val("")
		$("#remove").prop("checked", false)
		// Populate form
		$("#name").val($("#name"+pid).text())
		$("#email").val($("#email"+pid).text())
		$("#character").val($("#char"+pid).data("charid"))
		if($("#gm"+pid).data("gm") == "1")
			$("#gm").prop("checked", true)
		else
			$("#gm").prop("checked", false)
		// Set submit button text
		$("#btnSubmit").val("Update Player")
		// Title
		$("#aetitle").text("Edit "+$("#name"+pid).text())
		$("#aepassword").text("Reset")
		// Show "Remove player" checkbox
		$("#aeremove").show()
		// Show form
		$("#editform").show()
	})

	// Change Submit button text when Delete checkbox is ticked
	$("#remove").change(function (event) {
		if ($("#remove").prop("checked")) {
			$("#btnSubmit").val("Remove Player")
			$("#btnSubmit").css({backgroundColor: "firebrick", color: "white"})
		}
		else {
			$("#btnSubmit").val("Update Player")
			$("#btnSubmit").css({backgroundColor: "unset", color: "unset"})
		}
	})
	
	// Validate form
	$("#editform").submit(function (evt) {
		msg = ""
		
		if ($("#remove").prop("checked")) {
			if (!confirm("Click OK to confirm removal of " + $("#name").val()))
				msg = "Player removal cancelled"
		}
		else {
			// Do not check passwords when removing player
			if ($("#password").val().length > 0 && $("#password").val().length < 8) {
				if (msg != "")
					msg += "<br>"
				msg = "The new password must be at least eight characters long"
			}
			if ($("#password").val() != $("#password2").val()) {
				if (msg != "")
					msg += "<br>"
				msg += "The passwords do not match"
			}
		}
		
		if (msg != "") {
			// Show message and prevent form submission
			$("#msg").html(msg).show()
			evt.preventDefault()
		}
	})
})
</script>

<h1>Edit Player</h1>

<?php
if ($updated != "")
	echo "<p class='good'>$updated</p>";
?>
<p id="msg" class="bad hidden;"></p>

<?php
$sql = "SELECT playerid, players.name AS pname, email, gm, player_charid, characters.name AS cname
	FROM players
	LEFT JOIN characters
	ON player_charid = charid
	ORDER BY pname";
$players = $db->query($sql);
while ($player = $players->fetchArray (SQLITE3_ASSOC)) {
	$pid = $player["playerid"];
	echo "<div class='box character' style='padding-bottom:0px;'>
	<p class='boxtitle' id='name$pid'>".htmlentities($player["pname"], ENT_QUOTES)."</p><p>";
	
	if ($player["gm"] == 1)
		echo "<span id='gm$pid' data-gm='".$player["gm"]."'>GM</span><br>";
	else
		echo "<span style='display:none;' id='gm$pid' data-gm='".$player["gm"]."'></span>";
	
	echo "Email: <span id='email$pid'>".htmlentities($player["email"], ENT_QUOTES)."</span><br>";
	echo "Character: <span id='char$pid' data-charid='".$player["player_charid"]."'>";
	echo htmlentities($player["cname"], ENT_QUOTES)."<span>";
	echo "</p>
	<p><button class='editbutton' data-pid='".$player["playerid"]."'>Edit</button></p>
	</div>\n";
}
?>

<p>
<button id="addnewplayer">Add a new player</button>
</p>

<form method="post" id="editform" style="display:none;">
<input type="hidden" name="playerid" id="playerid">
<div class="box">
<p class="boxtitle" id="aetitle"></p>
<p>
Name (must be unique): <input name="name" required id="name" class="aeinput"><br>
Email: <input name="email" required id="email" type="email" class="aeinput"><br>
Character:
<select name="character" id="character">
<option value="0">No character</option>
<?php
selectCharacters($db, 0, 0);
?>
</select><br>
<input type="checkbox" name="gm" id="gm" value="1"> <label for="gm">This person is a GM</label>
</p>
<p>
<span id="aepassword">Set</span> password: <input type="password" name="password" id="password" class="aeinput"><br>
Confirm password: <input type="password" name="password2" id="password2" class="aeinput">
</p>
<p id="aeremove">
<input type="checkbox" name="remove" value="1" id="remove"> Remove this player
</p>
<p>
<input type="submit" value="Update Player" name="btnSubmit" id="btnSubmit">
</p>
</div>
</form>

<?php
require("inc_foot.php");
?>

<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/
require("inc_head_php.php");

if (isset($_GET["del"])) {
	// Delete from the DB tables
	$sql = "DELETE FROM groups WHERE groupid = ".intval($_GET["del"]);
	$db->exec($sql);
	$sql = "DELETE FROM groupmembers WHERE gm_groupid = ".intval($_GET["del"]);
	$db->exec($sql);
	// Redirect to page without del GET parameter
	header ("Location:".BASEURL."gm_groups.php");
}

require("inc_head_html.php");

// Add a new group
if (isset($_POST["btnAdd"]) && $_POST["btnAdd"] != "") {
	$sql = "INSERT INTO groups ('groupname')
		VALUES ('".$db->escapeString($_POST["name"])."')";
	$db->exec($sql);
}

// Rename an existing group
if (isset($_POST["btnRename"]) && $_POST["btnRename"] != "") {
	$sql = "UPDATE groups
		SET 'groupname' = '".$db->escapeString($_POST["name"])."'
		WHERE groupid = ".intval($_POST["group"]);
	$db->exec($sql);
}
?>

<script>
$(function() {
	$(".delete").click(function (event) {
		if (confirm("Are you sure you wish to delete this group?") == true)
			location.href = "<?=BASEURL;?>" + "gm_groups.php?del=" + $(this).data("id")
		else
			return false
	})
	$(".show").click(function (event) {
		if ($(this).text() == "Show members") {
			$("#group"+$(this).data("id")).show()
			$(this).text("Hide members")
		}
		else {
			$("#group"+$(this).data("id")).hide()
			$(this).text("Show members")
		}
	})
})
</script>

<h1>Add/Rename/Delete Groups</h1>

<h2>Groups</h2>
<?php
$sql = "SELECT * FROM groups ORDER BY groupname";
$groups = $db->query($sql);
while ($group = $groups->fetchArray(SQLITE3_ASSOC)) {
	$groupid = $group["groupid"];
	echo "<div class='box'>";
	echo "<p class='boxtitle'>".htmlentities($group["groupname"], ENT_QUOTES)."</p>";
	$sql = "SELECT COUNT(*) FROM groupmembers WHERE gm_groupid = $groupid";
	$num = $db->querySingle($sql);
	echo "<p>Members: $num<br>";
	echo "<button class='show' data-id='$groupid' id='btn$groupid'>Show members</button></p>";
	echo "<p id='group$groupid' style='display:none;'>";
	$sql = "SELECT charid, name FROM characters, groupmembers
		WHERE characters.charid = groupmembers.gm_charid
		AND groupmembers.gm_groupid = $groupid
		ORDER BY name";
	$chars = $db->query($sql);
	while ($char = $chars->fetchArray())
		echo htmlentities($char[1], ENT_QUOTES) . " [<a href='".BASEURL."gm_editchar.php?id=".$char[0]."'>Edit</a>]<br>";
	echo "</p>";
	echo "<p><button class='delete' data-id='$groupid'>Delete this group</button></p>";
	echo "</div>\n";
}
?>

<h2>Add/Rename Groups</h2>

<form method="post">
<div class="box">
<p class='boxtitle'>Add a Group</p>
<p>
Name: <input name="name" required class="mid">
<input type="submit" name="btnAdd" value="Add group">
</p>
</div>
</form>

<form method="post">
<div class="box">
<p class='boxtitle'>Rename a Group</p>
<p>
<?php
$sql = "SELECT COUNT(*) FROM groups";
if ($db->querySingle($sql) == 0)
	echo 'No groups have been created';
else {
	echo '<select name="group" style="margin-bottom:1ex;">';
	while ($group = $groups->fetchArray(SQLITE3_ASSOC))
		echo "<option value='".$group["groupid"]."'>".htmlentities($group["groupname"], ENT_QUOTES)."</option>";
	echo '</select><br>';
	echo 'New name: <input name="name" required class="mid">';
	echo '<input type="submit" name="btnRename" value="Rename">';
}
?>
</p>
</div>
</form>

<?php
require("inc_foot.php");
?>

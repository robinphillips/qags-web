<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/


/*
Authentication. This file provides built-in authentication against users stored in the SQLite database.
It may be replaced to authenticate against a different database, or to use LDAP, IMAP, etc.

This should define the following constants:
LOGINURL: URL of the login page
LOGOUTURL: URL to log out
ROLE: "player", "gm", or "guest"
PLAYERID: playerid from SQLite
CHARACTERID: ID of player's character in SQLite (or 0 if they don't have a character)
PLAYERNAME: The player's display name
EMAIL: player's email
*/

define ("LOGINURL", "login.php");
define ("LOGOUTURL", "login.php?action=logout");

// Delete expired logins
$sql = "DELETE FROM logins WHERE expire < ".time();
$db->exec ($sql);

// If token cookie exists, check against database
if (isset ($_COOKIE["qw_token"])) {
	// Get player ID
	$sql = "SELECT l_playerid FROM logins WHERE token = '".$db->escapeString($_COOKIE["qw_token"])."' AND expire > ".time();
	$l = $db->querySingle($sql);

	// If $l is False, Null, or zero-length array, there are no valid logins
	if ($l === False || is_null($l) || count($l) == 0) {
		// Token is not in the database or is invalid. Set as guest
		define ("ROLE", "guest");
		// Check for cookies. Blank them and set expire time to an hour ago so that they are deleted
		foreach ($_COOKIE as $key => $val)
			if (substr($key, 0, 3) == "qw_")
				setcookie ($key, "", time() - 3600);
	}
	else {
		// Valid login token found. Get player details
		$sql = "SELECT * FROM players WHERE playerid = $l";
		$p = $db->querySingle($sql, True);
		// Get role (player, guest, gm) and character ID
		if (count($p) > 0) {
			if ($p["gm"] == 1)
				define ("ROLE", "gm");
			else
				define ("ROLE", "player");
			define ("PLAYERID", $p["playerid"]);
			define ("CHARACTERID", $p["player_charid"]);
			define ("PLAYERNAME", $p["name"]);
			define ("EMAIL", $p["email"]);
			
			// Reset login expire times if "Stay logged in" was set
			if (isset ($_COOKIE["qw_stay"]) && $_COOKIE["qw_stay"] == 1) {
				$expire = time()+(60*60*24*30);
				setcookie ("qw_stay", 1, $expire);
				setcookie ("qw_token", $_COOKIE["qw_token"], $expire);
			}
		}
	}
}
else {
	// User is not logged in - set as guest
	define ("ROLE", "guest");
}

if (ROLE == "guest") {
	define ("PLAYERID", 0);
	define ("CHARACTERID", 0);
	define ("PLAYERNAME", "Guest");
	define ("EMAIL", "guest@example.com");
}
?>

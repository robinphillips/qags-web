<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

// Base URL, including trailing /
define ("BASEURL", "http://www.example.com/qags/");

// SQLite3 database file
define ("DBFILE", "res/qags.db");

// Site Name
define ("SITENAME", "QAGS Web");

// XMPP Settings. Set USE_XMPP to True to enable XMPP support
define ("USE_XMPP", False);
// Location of sendxmpp
define ("XMPP_EXE", "/usr/local/bin/sendxmpp");
// XMPP user and password
define ("XMPP_USER", "qags@example.com");
define ("XMPP_PASSWORD", "password");
// XMPP server, resource (alias), chatroom
define ("XMPP_SERVER", "example.com");
define ("XMPP_RESOURCE", "QAGS website");
define ("XMPP_CHATROOM", "qagschat@conference.example.com");

// IRC Settings. Set USE_IRC to True to enable IRC support
define ('USE_IRC', False);
// IRC server
define ('IRC_HOST', 'irc.example.com');
// IRC port
define ('IRC_PORT', 6667);
// IRC server password (if required)
define ('IRC_PASS', '');
// IRC channel
define ('IRC_CHAN', '#QAGS');
// IRC nick
define ('IRC_NICK', 'QAGS_Web');

// Array of extra links. Key is URL, value is text to display
$extlinks = array (
	"https://www.drivethrurpg.com/product/28315/QAGS-Second-Edition" => "QAGS Second Edition on Drive-Thru RPG",
);

// Array of GM-only extra links. Key is URL, value is text to display
$gm_extlinks = array (
	"http://www.hexgames.com/qags2e/" => "Hex Games",
);
?>

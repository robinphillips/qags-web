<!DOCTYPE html>
<html lang="en">
<head>
<!--
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
-->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" href="res/qags.css" type="text/css" media="screen">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<title><?=SITENAME;?></title>

<!-- Favicons -->
<link rel="icon" type="image/png" sizes="32x32" href="res/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="res/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="res/favicon-16x16.png">
<!-- Favicons -->

<script>
// Set window targets for links
$(function() {
	$("#linkselect").change(function(event) {
		if ($(this).find(':selected').data('type') == "internal")
			location.href = $(this).val()
		else
			window.open($(this).val(), '_blank')
	})
})
</script>
</head>
<body>
<span style="float:right;">
<select id="linkselect">
<?php
foreach ($links as $url => $label) {
	// Do not display GM-only links
	if (substr($url, 0, 3) != "gm_") {
		echo "<option data-type='internal' value='$url'";
		if (($_SERVER['QUERY_STRING'] == "" && basename($_SERVER['PHP_SELF']) == $url) ||
			basename($_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING']) == $url ||
			basename($_SERVER['PHP_SELF']) == $url) {
				echo " selected";
				$title = $label;
		}
		echo ">$label</option>";
	}
}

// GM-only links
if (ROLE == "gm") {
	echo "<optgroup label='GM-only links'>";
	foreach ($links as $url => $label) {
		// Pages named "gm_*" are GM-only
		if (substr($url, 0, 3) == "gm_") {
			echo "<option data-type='internal' value='$url' class='gmonlylink'";
			if (($_SERVER['QUERY_STRING'] == "" && basename($_SERVER['PHP_SELF']) == $url) ||
				basename($_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING']) == $url ||
				basename($_SERVER['PHP_SELF']) == $url) {
					echo " selected";
					$title = $label;
			}
			echo ">$label</option>";
		}
	}
}

if (count($extlinks)>0) {
	echo "<optgroup label='Extra links'>";
	foreach ($extlinks as $url => $label)
		echo "<option data-type='external' value='$url'>$label</option>";
	if (ROLE == "gm" && count($gm_extlinks)>0)
		foreach ($gm_extlinks as $url => $label)
			echo "<option data-type='external' class='gmonlylink' value='$url'>$label</option>";
	echo "</optgroup>";
}
?>
</select>
</span>

<script>
$(function() {
<?php
// Disable submit buttons for guests, except on login page
if (ROLE == "guest" && basename($_SERVER['PHP_SELF']) != "login.php")
	echo "$('input[type=\"submit\"]').prop('disabled', true)\n";
?>
	document.title = "<?=htmlentities($title, ENT_QUOTES) . " [" . SITENAME . "]";?>"
})
</script>

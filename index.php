<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require("inc_head_php.php");
require("inc_head_html.php");
?>
<script>
$(function() {
	$(".aEdit").click (function () {
		// Empty body content. This avoids character hide/show and lets user know that something is happening
		$("body").html("")
	})
})
</script>
<?php
function displayCharacter ($db, $pc) {
	$charid = $pc["charid"];
	echo "<div class='box character' data-charid='$charid'>";
	echo "<p class='boxtitle'>".htmlentities($pc["name"], ENT_QUOTES);
	if ($pc["plname"] != "")
		echo "&nbsp;<span class='sml'>(".htmlentities($pc["plname"], ENT_QUOTES).")</span>";
	if (ROLE == "gm")
		echo "<a class='sml' style='float:right;' href='gm_editchar.php?id=$charid' class='aEdit'>edit</a>";
	echo "</p>\n";
	if ($pc["concept"] != "")
		echo "<p>Concept: ".htmlentities($pc["concept"], ENT_QUOTES)."</p>";
	echo "<div class='innerhalf'>Body: ".intval($pc["body"])."<br>";
	echo "Brain: ".intval($pc["brain"])."<br>";
	echo "Nerve: ".intval($pc["nerve"])."</div>";
	echo "<div class='innerhalf'>Health: ".intval($pc["hp"])."<br>";
	echo "Current HP: ";
	if (intval($pc["currenthp"]) == intval($pc["hp"]))
		$hpstate = "good";
	elseif (intval($pc["currenthp"]) <= intval($pc["hp"])/2)
		$hpstate = "bad";
	else
		$hpstate = "warning";
	echo "<span class='$hpstate'>".intval($pc["currenthp"])."</span><br>";
	echo "Yum Yums: ".intval($pc["yumyums"])."</div>";

	$sql = "SELECT word, value FROM words WHERE type LIKE 'job' AND charid = $charid ORDER BY value DESC";
	$words = $db->query($sql);
	while ($word = $words->fetchArray(SQLITE3_ASSOC))
		echo "Job: ".htmlentities($word["word"], ENT_QUOTES)." (".intval($word["value"]).")<br>";

	echo "<div id='full$charid' class='fullchar' style='display:none;'>";
	echo "<p>";
	$sql = "SELECT word, value FROM words WHERE type LIKE 'gimmick' AND charid = $charid ORDER BY value DESC";
	$words = $db->query($sql);
	while ($word = $words->fetchArray(SQLITE3_ASSOC))
		echo "Gimmick: ".htmlentities($word["word"], ENT_QUOTES)." (".intval($word["value"]).")<br>";
	$sql = "SELECT word, value FROM words WHERE type LIKE 'weakness' AND charid = $charid ORDER BY value DESC";
	$words = $db->query($sql);
	while ($word = $words->fetchArray(SQLITE3_ASSOC))
		echo "Weakness: ".htmlentities($word["word"], ENT_QUOTES)." (".intval($word["value"]).")<br>";
	echo "</p>";

	$sql = "SELECT COUNT(*) FROM words WHERE type LIKE 'skill' AND charid = $charid";
	if ($db->querySingle($sql) > 0) {
		echo "<p>";
		$sql = "SELECT word, value FROM words WHERE type LIKE 'skill' AND charid = $charid ORDER BY value DESC";
		$words = $db->query($sql);
		while ($word = $words->fetchArray(SQLITE3_ASSOC))
			echo "Skill: ".htmlentities($word["word"], ENT_QUOTES)." (".intval($word["value"]).")<br>";
		echo "</p>";
	}

	echo "<p>";
	if ($pc["tagline"] != "")
		echo "Tag Line: &quot;<i>".htmlentities($pc["tagline"], ENT_QUOTES)."</i>&quot;<br>";
	if ($pc["dumbfact"] != "")
		echo "Dumb Fact: ".htmlentities($pc["dumbfact"], ENT_QUOTES)."<br>";
	if ($pc["wwphitm"] != "") {
		echo "WWPHITM: ".htmlentities($pc["wwphitm"], ENT_QUOTES);
		echo " - <a class='sml' target='_blank' href='http://www.imdb.com/find?s=nm;exact=true;q=".urlencode($pc["wwphitm"])."'>IMDB search</a>";
	}
	echo "</p>";

	if ($pc["notes"] != "")
		echo "<p>Notes:<br>".nl2br(htmlentities($pc["notes"], ENT_QUOTES))."</p>";
	if ($pc["gmnotes"] != "" && ROLE == "gm")
		echo "<p class='gmonlyblock'>GM Notes:<br>".nl2br(htmlentities($pc["gmnotes"], ENT_QUOTES))."</p>";
	
	echo "</div></div>\n";
}

function displayCharacters($db, $gmc) {
	// Display player's character if they have one
	$sql = "SELECT characters.*, players.name AS plname
		FROM characters
		LEFT JOIN players ON charid=players.player_charid
		WHERE gmc = $gmc
		AND active = 1
		AND charid = ".CHARACTERID;
	$pc = $db->querySingle($sql, True);
	if (count($pc) > 0)
		displayCharacter($db, $pc);

	// Display other characters
	$sql = "SELECT characters.*, players.name AS plname
		FROM characters
		LEFT JOIN players ON charid=players.player_charid
		WHERE gmc = $gmc
		AND active = 1
		AND charid <> ".CHARACTERID."
		ORDER BY name";
	$pcs = $db->query($sql);
	while ($pc = $pcs->fetchArray(SQLITE3_ASSOC))
		displayCharacter($db, $pc);
}
?>

<script>
$(function() {
	$(".character").click(function() {
		$("#full"+$(this).data("charid")).toggle()
	})
	
	$("#showAll").click(function() {
		if ($("#showAll").text() == "Show details of all characters") {
			$("#showAll").text("Hide details of all characters")
			$(".fullchar").show()
		}
		else {
			$("#showAll").text("Show details of all characters")
			$(".fullchar").hide()
		}
	})
})
</script>

<h1>Active Player Characters</h1>

<?php
if (isset($_GET["msg"]) && $_GET["msg"] == "notloggedin")
	echo "<p class='bad'>You are not logged in, or you do not have permission to access that page. <a href='".BASEURL."login.php'>Log in</a>.</p>\n";
if (isset($_GET["msg"]) && $_GET["msg"] == "guest")
	echo "<p class='bad'>Guests cannot view that page. <a href='".BASEURL."login.php'>Log in</a>.</p>\n";
?>

<p>
Click on a character to show/hide details. <a href="#" id="showAll">Show details of all characters</a>.
</p>

<?php
displayCharacters($db, 0);

if (ROLE == "gm") {
?>

<h1>Active GMCs</h1>

<?php
displayCharacters($db, 1);
}

require("inc_foot.php");
?>

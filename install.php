<!DOCTYPE html>
<html lang="en">
<head>
<!--
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
-->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" href="res/qags.css" type="text/css" media="screen">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<title>INSTALL [QAGS Web]</title>
<script>
$(function() {
	$("#gmform").submit(function (evt) {
		msg = ""
		
		if ($("#password").val().length < 8) {
			if (msg != "")
				msg += "<br>"
			msg = "The password must be at least eight characters long"
		}
		if ($("#password").val() != $("#password2").val()) {
			if (msg != "")
				msg += "<br>"
			msg += "The passwords do not match"
		}
		
		if (msg != "") {
			// Show message and prevent form submission
			$("#msg").html(msg).show()
			evt.preventDefault()
		}
	})
})
</script>
</head>
<body>

<?php
require ("inc_config.php");
?>

<h1>Install QAGS Web</h1>

<p id="msg" class="bad hidden;"></p>

<?php
// Show warning if database file already exists
if (file_exists(DBFILE))
	echo "<p class='bad'>The database file already exists. If you are re-installing, delete the database file (<tt>".DBFILE."</tt>) first.</p>\n";

// Function to create a database table
function createtable ($db, $tablename, $sql, &$bSuccess) {
	echo "Database: creating '$tablename' table. ";
	
	try {
		if ($db->exec($sql) == True)
			echo " <span class='good'>Success!</span>";
		else {
			echo " <span class='bad'>Failed!</span>";
			$bSuccess = False;
		}
	}
	catch (Exception $e) {
		echo " <span class='bad'>Failed!</span>";
		$bSuccess = False;
	}
	
	echo "<br>\n";
}

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	try {
		$db = new SQLite3(DBFILE);
	}
	catch (Exception $e) {
		echo "<p class='bad'>Cannot create database file. Ensure web server has write permissions on the directory</p>\n";
	}

	$bSuccess = True;
	echo "<p>\n";
	
	// Create database tables
	$sql = "CREATE TABLE 'characters' ('charid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'gmc' BOOLEAN DEFAULT 1 , 'name' TEXT, 'body' INTEGER, 'brain' INTEGER, 'nerve' INTEGER,'hp' INTEGER, 'yumyums' INTEGER, 'concept' TEXT, 'tagline' TEXT, 'dumbfact' TEXT, 'notes' TEXT, 'wwphitm' TEXT, 'active' BOOLEAN DEFAULT 1 , 'currenthp' INTEGER)";
	createtable ($db, "characters",  $sql, $bSuccess);

	$sql = "CREATE TABLE 'log' ('logid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'datetime' DATETIME DEFAULT CURRENT_TIMESTAMP, 'log' TEXT)";
	createtable ($db, "log", $sql, $bSuccess);
	
	$sql = "CREATE TABLE 'players' ('playerid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'name' TEXT, 'player_charid' INTEGER DEFAULT 0, 'password' TEXT, 'gm' BOOLEAN DEFAULT 0, 'email' TEXT, 'reset' TEXT DEFAULT NULL)";
	createtable ($db, "players", $sql, $bSuccess);
	
	$sql = "CREATE TABLE 'words' ('wordid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'type' TEXT, 'charid' INTEGER, 'word' TEXT, 'value' INTEGER)";
	createtable ($db, "words", $sql, $bSuccess);

	$sql = "CREATE TABLE 'logins' ('l_playerid' INTEGER, 'token' TEXT, 'expire' INTEGER)";
	createtable ($db, "logins", $sql, $bSuccess);

	$sql = "CREATE TABLE 'availability' ('avid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'playerday' TEXT, 'colour' TEXT)";
	createtable ($db, "availability", $sql, $bSuccess);

	$sql = "CREATE TABLE 'config' ('confid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'key', 'val')";
	createtable ($db, "config", $sql, $bSuccess);
	$sql = "INSERT INTO config (key, val) VALUES ('av_date', '')";
	$db->exec ($sql);

	// Set up the GM user
	echo "Database: creating GM user.";
	$sql = "INSERT INTO players (name, password, gm, email)
		VALUES (
		'".$db->escapeString($_POST["name"])."',
		'".$db->escapeString(password_hash ($_POST["password"], PASSWORD_DEFAULT))."',
		1,
		'".$db->escapeString($_POST["email"])."')";
	if ($db->exec($sql))
		echo " <span class='good'>Success!</span>";
	else {
		echo " <span class='bad'>Failed!</span>";
		$bSuccess = False;
	}
	echo "</p>\n";
	
	if ($bSuccess)
		echo "<p class='good'>Database created successfully.</p>\n";
	else
		echo "<p class='bad'>Errors creating database.</p>\n";
}
else {
?>
	<p>
	To install QAGS Web, edit the <tt>inc_config.php</tt> file as required. Then fill in the details below and click the <b>Install</b> button.
	</p>

	<p>
	<form action="install.php" method="post" id="gmform">
	GM Name: <input name="name" required id="name"><br>
	GM Email: <input name="email" required id="email" type="email"><br>
	GM Password: <input type="password" name="password" id="password"><br>
	Confirm GM password: <input type="password" name="password2" id="password2">
	</p>
	<p>
	<input name="btnSubmit" type="submit" value="Install">
	</p>
	</form>
<?php
}
?>

<p>
<a href="index.php">Home</a>
</p>

</body>
</html>

<?php
$db->close();
?>

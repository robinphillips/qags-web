<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require("inc_head_php.php");
// Redirect guests to index page
if (ROLE == "guest")
	header("Location:".BASEURL);

require("inc_head_html.php");

$updated = "";
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	$sql = "UPDATE players SET
		name = '".$db->escapeString($_POST["name"])."',
		email = '".$db->escapeString($_POST["email"])."',";

	// Only change password if one was set
	if ($_POST["password"] != "")
		$sql .= "password = '".$db->escapeString(password_hash ($_POST["password"], PASSWORD_DEFAULT))."',";

	$sql .= "player_charid = ".$_POST["character"]."
		WHERE playerid=".PLAYERID;
	$db->exec($sql);
	$updated = "Profile updated.";
}

$sql = "SELECT * FROM players WHERE playerid = ".PLAYERID;
$player = $db->querySingle($sql, True);
?>

<script>
$(function() {
	$("#profileform").submit(function (evt) {
		msg = ""
		
		if ($("#password").val().length > 0 && $("#password").val().length < 8) {
			if (msg != "")
				msg += "<br>"
			msg = "The new password must be at least eight characters long"
		}
		if ($("#password").val() != $("#password2").val()) {
			if (msg != "")
				msg += "<br>"
			msg += "The passwords do not match"
		}
		
		if (msg != "") {
			$("#msg").html(msg).show()
			evt.preventDefault()
		}
	})
})
</script>

<h1>Profile</h1>

<?php
if ($updated != "")
	echo "<p class='good'>$updated</p>";
?>
<p id="msg" class="bad hidden;"></p>

<form method="post" id="profileform">
<div class="box">
<p>
Name: <input name="name" required id="name" value="<?=htmlentities($player["name"], ENT_QUOTES);?>"><br>
Email: <input name="email" required id="email" type="email" value="<?=htmlentities($player["email"], ENT_QUOTES);?>"><br>
Character:
<select name="character">
<option value="0">No character</option>
<?php
selectCharacters($db, $player["player_charid"], 0);
?>
</select>
</p>
<p>
Set new password: <input type="password" name="password" id="password"><br>
Confirm password: <input type="password" name="password2" id="password2">
</p>
<p>
<input type="submit" value="Update Profile" name="btnSubmit">
</p>
</div>
</form>

<?php
require("inc_foot.php");
?>

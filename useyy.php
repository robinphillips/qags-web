<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require("inc_head_php.php");
require("inc_head_html.php");
// Initialise $log
$log = "";
if (isset($_POST["characterid"]))
	$charid = intval($_POST["characterid"]);
else
	$charid = CHARACTERID;

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	$sql = "SELECT charid, name, yumyums FROM characters WHERE charid = $charid";
	$char = $db->querySingle($sql, True);

	$yumyums = intval($char["yumyums"]) - abs(intval($_POST["yumyums"]));
	$sql = "UPDATE characters SET yumyums = $yumyums WHERE charid = $charid";
	$db->exec($sql);
	
	if (abs(intval($_POST["yumyums"])) == 1)
		$plural = "";
	else
		$plural = "s";
	
	$log = $char["name"] . " has used " . abs(intval($_POST["yumyums"])) . " Yum Yum$plural, and has $yumyums remaining";

	// Log the result
	logdb ($log);
}

$sql = "SELECT charid, name, yumyums FROM characters WHERE charid = $charid";
$char = $db->querySingle($sql, True);
?>

<script>
$(function() {
	$("#yumyums").change(function() {
		start = parseInt($("#yystart").text())
		touse = parseInt($(this).val())
		// Validate value
		if ($.isNumeric(touse) == false)
			touse = 0
		if (touse < 0) {
			touse = 0
			$(this).val(0)
		}

		// Check number is not too high
		if (touse > start) {
			$(this).css ("border", "thin red solid")
			$("#btnSubmit").prop ("disabled", true)
		}
		else {
			$(this).css ("border", "")
			$("#btnSubmit").prop ("disabled", false)
		}
		
		// Update number remaining
		$("#yyremain").text (start - touse)
	})

	$("#character").change(function() {
		$("#yystart").text(characters[$(this).val()])
		$("#yumyums").change()
	})
	
	// Initialise
	$("#character").change()
})
</script>

<h1>Use Yum Yums</h1>

<?php
if (isset($char["name"]) || ROLE == "gm") {
?>

<form method="post">
<p>Character:

<?php
if (ROLE == "gm") {
	// Set up JavaScript array to hold character YYs
	echo "\n<script>\ncharacters = new Array()\n";
	$sql = "SELECT charid, yumyums FROM characters WHERE active LIKE 1";
	$chars = $db->query($sql);
	while ($ch = $chars->fetchArray())
		echo "characters[".$ch["charid"]."] = ".$ch["yumyums"]."\n";
	echo "\n</script>\n";

	// Drop-down to select character
	echo "<select id='character' name='characterid'>";
	selectCharacters($db);
	echo "</select>";
}
else {
	echo "<input type='hidden' name='characterid' value='".CHARACTERID."'>";
	echo htmlentities($char["name"], ENT_QUOTES);
}
?>

</p>

<p>Your character currently has <span id="yystart"><?=intval($char["yumyums"]);?></span> Yum Yums</p>

<p>Number of Yum Yums to use: <input name="yumyums" class="small" id="yumyums" type="number"></p>

<p>Number of Yum Yums remaining: <span id="yyremain"><?=intval($char["yumyums"]);?></span></p>

<input type="submit" name="btnSubmit" id="btnSubmit" value="Use Yum Yums">
</form>

<?php
}
else {
	echo "<p>You do not have a character defined.</p>\n";
}

if ($log != "")
	echo "<p>$log</p>";

require("inc_foot.php");
?>
